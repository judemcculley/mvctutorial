namespace MVCTutorial.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MVCTutorial.Models.MVCTutorialContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "MVCTutorial.Models.MVCTutorialContext";
        }

        protected override void Seed(MVCTutorial.Models.MVCTutorialContext context)
        {
            context.Contacts.AddOrUpdate(
                p => p.Id,
                new Models.Contact { Id = 1, Birthday = new DateTime(1920, 01, 20), City = "Chicago"
                                    , Email = "doc.mccoy@starfleet.com", FirstName = "DeForest", LastName = "Kelley"
                                    , PhonePrimary = "123-456-7890", PhoneSecondary = "234-567-8901", State = "IL"
                                    , StreetAddress = "Sickbay, Starship Enterprise NCC-1701"
                                    , UserId = new Guid("cdd5afd4-38f0-4214-aa64-93023303bddf")
                                    , Zip = "98765" } 
                , new Models.Contact { Id = 2, Birthday = new DateTime(1920, 03, 03), City = "New York"
                                    , Email = "i.beam.you.up@starfleet.com", FirstName = "James", LastName = "Doohan"
                                    , PhonePrimary = "345-678-9012", PhoneSecondary = "456-789-0123", State = "NY"
                                    , StreetAddress = "Engineering, Starship Enterprise NCC-1701"
                                   , UserId = new Guid("10569695-74b9-4907-9a83-5fc4488bca2e")
                                    , Zip = "87654" }
                , new Models.Contact { Id = 3, Birthday = new DateTime(1931, 03, 26), City = "Los Angeles"
                                    , Email = "its.only.logic@starfleet.com", FirstName = "Leonard", LastName = "Nimoy"
                                    , PhonePrimary = "987-654-3210", PhoneSecondary = "876-543-2109", State = "CA"
                                    , StreetAddress = "Science Station 1, Starship Enterprise NCC-1701"
                                    , UserId = new Guid("cdd5afd4-38f0-4214-aa64-93023303bddf")
                                    , Zip = "76543-2109" }
                , new Models.Contact { Id = 4, Birthday = new DateTime(1931, 03, 22), City = "Riverside"
                                    , Email = "the.captain@starfleet.com", FirstName = "William", LastName = "Shatner"
                                    , PhonePrimary = "765-432-1098", PhoneSecondary = "654-321-0987", State = "IA"
                                    , StreetAddress = "The Bridge, Starship Enterprise NCC-1701"
                                    , UserId = new Guid("10569695-74b9-4907-9a83-5fc4488bca2e")
                                    , Zip = "65432-0123" }
            );
        }
    }
}
